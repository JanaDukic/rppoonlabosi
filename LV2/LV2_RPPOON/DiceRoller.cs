﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_RPPOON
{
    //primjer2 //primjer3(logger)
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger ilogger;
        //private Logger logger;

        //public DiceRoller()
        //{
        //    this.dice = new List<Die>();
        //    this.resultForEachRoll = new List<int> ();
        //    this.logger = new Logger("Console", null);
        //}

        //zadatak4
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.ilogger = new ConsoleLogger();
        }
        public DiceRoller(ILogger ilogger)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.ilogger = ilogger;
        }
        

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RollAllDice()
        {
            //clear results of previus rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                int br = die.Roll();
                this.resultForEachRoll.Add(br);
            }
        }

        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }
        //zadatak 4
        //public void LogRollingResults()
        //{
        //    StringBuilder stringbilder = new StringBuilder();
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        stringbilder.Append( result.ToString()).Append("\n");
        //    }
        //    ilogger.Log(stringbilder.ToString());
        //}

        //primjer3
        //public void LogRollingResults()
        //{
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        logger.Log(result.ToString());
        //    }
        //}

        //zadatak5
        public string GetStringRepresentation()
        {
            StringBuilder stringbilder = new StringBuilder();
            foreach (int result in this.resultForEachRoll)
            {
                stringbilder.Append(result.ToString()).Append("\n");
            }
            return stringbilder.ToString();
        }

        public void LogRollingResults()
        {
           ilogger.Log(this);
        }
    }
}
