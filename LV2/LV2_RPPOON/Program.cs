﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //zadatak1
            //Console.WriteLine("zadatak 1:");
            //DiceRoller roller1 = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die1 = new Die(6);
            //    roller1.InsertDie(die1);
            //}
            //roller1.RollAllDice();
            //printResults(roller1);

            ////zadatak2
            //Console.WriteLine("zadatak 2:");
            //Random random = new Random();
            //DiceRoller roller2 = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die2 = new Die(6, random);
            //    roller2.InsertDie(die2);
            //}
            //roller2.RollAllDice();
            //printResults(roller2);

            //zadatak3
            //Console.WriteLine("zadatak 3:");
            //DiceRoller roller3 = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die3 = new Die(6);
            //    roller3.InsertDie(die3);
            //}
            //roller3.RollAllDice();
            //printResults(roller3);

            //zadatak4
            //Console.WriteLine("zadatak 4: -> deafulatni(console)");
            //DiceRoller roller4 = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die4 = new Die(6);
            //    roller4.InsertDie(die4);
            //}
            //roller4.RollAllDice();
            ////printResults(roller4);
            //roller4.LogRollingResults();

            //Console.WriteLine("zadatak 4: -> fileLogger(filepath)");
            //FileLogger loger = new FileLogger("C:\\Users\\Korisnik\\source\\repos\\LV2_RPPOON\\LV2_RPPOON\\text.txt");
            //DiceRoller roller5 = new DiceRoller(loger);
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die5 = new Die(6);
            //    roller5.InsertDie(die5);
            //}
            //roller5.RollAllDice();
            //printResults(roller5);
            //roller5.LogRollingResults();

            //zadatak5
            //Console.WriteLine("zadatak 5:");
            //ConsoleLogger logerConsole = new ConsoleLogger();
            //FileLogger logerFile = new FileLogger("C:\\Users\\Korisnik\\source\\repos\\LV2_RPPOON\\LV2_RPPOON\\peti.txt");
            //DiceRoller roller6 = new DiceRoller(logerConsole);
            //DiceRoller roller7 = new DiceRoller(logerFile);
            //for (int i = 0; i < 20; i++)
            //{
            //    Die die6 = new Die(6);
            //    roller6.InsertDie(die6);
            //    roller7.InsertDie(die6);
            //}
            //roller6.RollAllDice();
            //roller7.RollAllDice();

            //roller6.LogRollingResults();
            //roller7.LogRollingResults();
            //printResults(roller7); //za usporedbu s peti.txt

            //zadatak7
            Console.WriteLine("zadatak7: ");
            FlexibleDiceRoller roller8 = new FlexibleDiceRoller();
            for (int i = 0; i < 10; i++)
            {
                Die die8 = new Die(6);
                roller8.InsertDie(die8);
            }
            for (int i = 0; i < 10; i++)
            {
                Die die9 = new Die(4);
                roller8.InsertDie(die9);
            }
            roller8.RollAllDice();
            roller8.PrintDices();
            roller8.RemoveSameAsGiven(4);
            Console.WriteLine("Nakon removanja: ");
            roller8.PrintDices();
            

        }

        static public void printResults(DiceRoller roller)
        {
            IList<int> list = roller.GetRollingResults();
            foreach (int result in list)
            {
                Console.WriteLine(result);
            }
        }
        
        
    }
}
