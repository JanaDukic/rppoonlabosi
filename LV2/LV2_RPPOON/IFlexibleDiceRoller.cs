﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_RPPOON
{
    //zadatak6
    interface IFlexibleDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
