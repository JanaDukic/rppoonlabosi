﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak1
{
    class Adapter: IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            
            double[][] convertingResult = new double[dataset.GetData().Count][];

            for(int i=0; i<dataset.GetData().Count; i++)
            {
                double[] data = new double[dataset.GetData()[i].Count];
                for(int j=0; j<dataset.GetData()[i].Count; j++)
                {
                    data[j] = dataset.GetData()[i][j];
                }
                convertingResult[i] = data;
            }
            for (int i = 0; i < dataset.GetData().Count; i++)
            {
                for (int j = 0; j < dataset.GetData()[i].Count; j++)
                {
                   // Console.WriteLine(convertingResult[i][j]);
                }

                //Console.WriteLine("\n");
            }
            return convertingResult; 
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
