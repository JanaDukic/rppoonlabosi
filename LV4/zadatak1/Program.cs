﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Korisnik\\source\\repos\\LV4_RPPOON\\zadatak1\\CSVexample.txt");
            Analyzer3rdParty analizer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analizer);
            adapter.CalculateAveragePerRow(dataset);
            adapter.CalculateAveragePerColumn(dataset);
        }
    }
}
