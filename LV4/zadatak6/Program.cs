﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordValidator password = new PasswordValidator(8);
            Console.WriteLine(password.IsValidPassword("ovonijedobrasifra"));
            Console.WriteLine(password.IsValidPassword("ovojeskoro123super"));
            Console.WriteLine(password.IsValidPassword("ovaJe2top"));

            EmailValidator email = new EmailValidator();
            Console.WriteLine(email.IsValidAddress("jana@banako.tcom"));
            Console.WriteLine(email.IsValidAddress("jana@banakohr"));
            Console.WriteLine(email.IsValidAddress("janabanako.com"));
            Console.WriteLine(email.IsValidAddress("jana@gmail.hr"));


        }
    }
}
