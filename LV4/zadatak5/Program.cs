﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            Video movie = new Video("Mullan");
            Book book = new Book("Pride and Prejudice");
            Video movie2 = new Video("Pocahontas");
            Book book2 = new Book("Romeo and Juliet");
            HotItem hotmovie = new HotItem(movie2);
            HotItem hotbook = new HotItem(book2);
            List<IRentable> list = new List<IRentable>();
            list.Add(movie);
            list.Add(book);
            list.Add(hotmovie);
            list.Add(hotbook);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);

            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem MullanMovie = new DiscountedItem(movie,10);
            DiscountedItem PandPBook = new DiscountedItem(book,10);
            DiscountedItem PocahontasMovie = new DiscountedItem(movie2,10);
            DiscountedItem RandJBook = new DiscountedItem(book2,10);
            flashSale.Add(MullanMovie);
            flashSale.Add(PandPBook);
            flashSale.Add(PocahontasMovie);
            flashSale.Add(RandJBook);

            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);

        }
    }
}
