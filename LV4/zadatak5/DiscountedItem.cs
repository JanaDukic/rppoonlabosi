﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class DiscountedItem: RentableDecorator
    {
        private readonly double DiscountPercentage;

        public DiscountedItem(IRentable rentable, double discoundpercentage) : base(rentable)
        {
            this.DiscountPercentage = discoundpercentage;
        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() - (this.DiscountPercentage/100)*base.CalculatePrice();
        }

        public override string Description
        {
            get
            {
                return base.Description +" now at [" +this.DiscountPercentage + "]% off!" ;
            }
        }
    }
}
