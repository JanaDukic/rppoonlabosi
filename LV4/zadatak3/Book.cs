﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Book: IRentable
    {
        public String Title { get; private set; }
        private readonly double BaseBookPrice = 3.99;

        public Book(String title) { this.Title = title; }
        public string Description { get{ return this.Title; } }
        public double CalculatePrice() { return BaseBookPrice; }

    }
}
