﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            Video movie = new Video("Mullan");
            Book book = new Book("Pride and Prejudice");
            List<IRentable> list = new List<IRentable>();
            list.Add(movie);
            list.Add(book);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);

        }
    }
}
