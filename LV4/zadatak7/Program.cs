﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                UserEntry entry = UserEntry.ReadUserFromConsole();
                Validation registrationValidation = new Validation();
                if(registrationValidation.IsUserEntryValid(entry)== false)
                {
                    break;
                }
            }
        }
    }
}
