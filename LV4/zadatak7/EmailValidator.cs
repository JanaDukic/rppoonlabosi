﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class EmailValidator : IEmailValidatorService
    {
        public EmailValidator() { }

        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsAllEsentialTypes(candidate);
        }


        private bool ContainsAllEsentialTypes(String candidate)
        {
            bool hasMonkey = false, hasCom = false, hasHr = false;
            if (candidate.Contains("@")) hasMonkey = true;
            if (candidate.EndsWith(".com")) hasCom = true;
            if (candidate.EndsWith(".hr")) hasHr = true;
          
          
            return (hasMonkey && (hasCom || hasHr));

        }
    }
}
