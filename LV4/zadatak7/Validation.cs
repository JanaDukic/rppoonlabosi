﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Validation : IRegistrationValidater
    {
        IEmailValidatorService emailValidator ;
        IPasswordValidatorService passwordValidator;

        public Validation()
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(8);

        }
       
        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password);  
        }
    }
}
