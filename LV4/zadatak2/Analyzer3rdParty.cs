﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for(int i=0; i<rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }

        public double[] PerColumnAverage(double[][] data)
        {
            int rowCount = data.Length;
         
            int columnCount = data[1].Length;
           
            double[] results = new double[columnCount];
            for(int j=0; j<columnCount; j++)
            {
                double sum = 0;
                for (int i = 0; i < rowCount; i++)
                {
                    sum += data[i][j];
                }
                results[j] = sum / rowCount;
             
            }
            return results;
        }
    }
}
