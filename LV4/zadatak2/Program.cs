﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Korisnik\\source\\repos\\LV4_RPPOON\\zadatak1\\CSVexample.txt");
            Analyzer3rdParty analizer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analizer);
            double[] rowaverage = adapter.CalculateAveragePerRow(dataset);
            double[] columnaverage = adapter.CalculateAveragePerColumn(dataset);
            StringBuilder builder = new StringBuilder();
            Console.WriteLine("Results Per Row: ");
            for (int i = 0; i < rowaverage.Length; i++)
            {
                Console.WriteLine(rowaverage[i]);
            }
            Console.WriteLine("Results Per Column: ");
            for (int i = 0; i < columnaverage.Length; i++)
            {
                Console.WriteLine(columnaverage[i]);
            }

        }
    }
}
