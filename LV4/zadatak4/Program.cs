﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            Video movie = new Video("Mullan");
            Book book = new Book("Pride and Prejudice");
            Video movie2 = new Video("Pocahontas");
            Book book2 = new Book("Romeo and Juliet");
            HotItem hotmovie = new HotItem(movie2);
            HotItem hotbook = new HotItem(book2);
            List<IRentable> list = new List<IRentable>();
            list.Add(movie);
            list.Add(book);
            list.Add(hotmovie);
            list.Add(hotbook);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);

            //razlika je u tome što se sad na osnovnu cijenu hot knjige i hot filma nadodaje još 1.99 recimo kuna
            //te također za hot items imamo overridanu metodu tostring pa baš naznači koji od njih je hot
        }
    }
}
