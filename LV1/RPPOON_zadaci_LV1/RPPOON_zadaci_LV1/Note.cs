﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_zadaci_LV1
{
    class Note
    {
        //zd2
        private String text;
        private String author;
        private int importance;

        //levels of importance: 0-irrelevant, 1-a little important,2-super important

        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getImportnace() { return this.importance; }
        public void setText(string text) { this.text = text; }
        public void setImportance(int importance) { this.importance = importance; }

        //zd3
        public Note() {
            this.text = "Blank page";
            this.author = "No author";
            this.importance = -1;
        }
        public Note(string text, string author, int importance) {
            this.text = text;
            this.author = author;
            this.importance = importance;
        }
        public Note(Note reference)
        {
            this.text = reference.text;
            this.author = reference.author;
            this.importance = reference.importance;
        }

        //zd4
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Importnace
        {
            get { return this.importance; }
            set { this.importance = value; }
        }

        //zd5
        public override string ToString()
        {
            return "Text: " + this.Text + " ,Author: " + this.Author + " ,Importance: " + this.Importnace;
            
        }
    }
}
