﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_zadaci_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            //zd3
            Note firstNote = new Note();
            Note secondNote = new Note("Remeber to clean windows today","Jana Dukic",2);
            Note thirdNote = new Note(secondNote);

            Console.WriteLine("zd3:");
            Console.WriteLine("First note text and author:");
            Console.WriteLine(firstNote.getText() + " ," +firstNote.getAuthor());
            Console.WriteLine("Second note text and author:");
            Console.WriteLine(secondNote.getText() + " ," + secondNote.getAuthor());
            Console.WriteLine("Third note text and author:");
            Console.WriteLine(thirdNote.getText() + " ," + thirdNote.getAuthor());

            //zd4
            Console.WriteLine("\nzd4:");
            Console.WriteLine("First note text and author:");
            Console.WriteLine(firstNote.Text + " ," + firstNote.Author);

            //zd 5
            Console.WriteLine("\nzd5:");
            Console.WriteLine("Second note text and author:");
            Console.WriteLine(secondNote.ToString());

            //zd6
            Console.WriteLine("\nzd6:");
            TimeStamped firstTimeStamped = new TimeStamped("Work out!","David Dukic",1);
            Console.WriteLine(firstTimeStamped.ToString());

            //zd7
            Console.WriteLine("\nzd7:");
            ToDo notes = new ToDo(3);
        
            string text;
            string author;
            int importance;
            for (int i = 0; i < 3; i++)
            {
                text = Console.ReadLine();
                author = Console.ReadLine();
                importance = Convert.ToInt32(Console.ReadLine());

                TimeStamped hardnote = new TimeStamped(text, author, importance);
                notes.Add(hardnote);
            }
            Console.WriteLine(notes.ToString());
            notes.Add(firstTimeStamped);
            Console.WriteLine("\n"+notes.ToString());
            notes.RemoveByImportance();
            Console.WriteLine("\n"+notes.ToString());
            Console.WriteLine("\n" +notes.getByIndex(1));

        }
    }
}
