﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_zadaci_LV1
{
    class TimeStamped: Note
    {   
        //zd6
        private DateTime time;

        public TimeStamped(string text, string author, int importance) : base(text, author, importance)
        {
            this.time = DateTime.Now;
        }

        public TimeStamped(string text, string author, int importance, DateTime time): base(text,author, importance)
        {
            this.time = time;
        }
        
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public override string ToString()
        {
            return "Text: " + this.Text + " ,Author: " + this.Author + " ,Importance: " + this.Importnace + " ,Time: " + this.Time;
        }
    }
}
