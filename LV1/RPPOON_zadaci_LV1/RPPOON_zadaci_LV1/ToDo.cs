﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_zadaci_LV1
{
    class ToDo
    {
        //zd7
        private List<TimeStamped> todo;

        public ToDo(int size)
        {
            todo = new List<TimeStamped>(size);
        }

        public List<TimeStamped> Add( TimeStamped theNote)
        {
            todo.Add(theNote);
            return todo;
        }

        public void RemoveAll()
        {
            for(int i= todo.Count()-1; i>=0; i--)
            {
                todo.RemoveAt(i);
            }
        }

        public List<TimeStamped> RemoveByImportance()
        {
            for(int i=todo.Count()-1; i>=0; i--)
            {
                if(2 == todo.ElementAt(i).Importnace)   //2 is the super important
                {
                    todo.RemoveAt(i);
                }
            }
            return todo;
        }

        public TimeStamped getByIndex( int index)
        {
            return todo[index-1];
        }

        public override string ToString()
        {
            string completeToDoList = "";
            for(int i = 0; i<= todo.Count() - 1; i++)
            {
                completeToDoList +=  "\n" +todo[i].ToString();
            }
            return completeToDoList;
        }
    }
}
