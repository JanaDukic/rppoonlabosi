﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product hairproduct = new Product("Hairbrush", 90, 0.1);
            Product bagproduct = new Product("Target bag", 150, 0.3);

            Box box = new Box("nice box");
            box.Add(hairproduct);
            box.Add(bagproduct);

            Console.WriteLine(box.Description());
            ShippingService Tisak = new ShippingService(4.99);
            Console.WriteLine(Tisak.CalculateShippingPrice(box));

        }
    }
}
