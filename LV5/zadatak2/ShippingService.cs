﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class ShippingService
    {
        private double unitprice;

        public ShippingService(double unitprice)
        {
            this.unitprice = unitprice;
        } 

        public double CalculateShippingPrice(IShipable product)
        {
            double shippingPrice = 0.0;
            shippingPrice = product.Weight*unitprice;
            return shippingPrice;
        }
    }
}
