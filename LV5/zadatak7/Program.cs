﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();

            LightTheme lighttheme = new LightTheme();
            string message = "You have to put LV5 on git!\n";
            ReminderNote remindernote = new ReminderNote(message, lighttheme); 

            
            string message1 = "This year Fitness members are: \n";
            FitnessGroupNote fitnesgroup = new FitnessGroupNote(message1, lighttheme);
            fitnesgroup.AddAttendant("Jana");
            fitnesgroup.AddAttendant("David");
            fitnesgroup.RemoveAttendant("Jana");
            fitnesgroup.AddAttendant("Sven");
 
            DarkMagentaTheme darkomodo = new DarkMagentaTheme();
            string message2 = "Bring your best friend to fitness training! \n";
            FitnessGroupNote fitnesandfriends = new FitnessGroupNote(message2, darkomodo);
            fitnesandfriends.AddAttendant("Jana i Rebeka");
            fitnesandfriends.AddAttendant("David i Danijel");
            fitnesandfriends.AddAttendant("Sven i Ivor");
            fitnesandfriends.RemoveAttendant("Sven i Ivor");

            notebook.AddNote(remindernote);
            notebook.AddNote(fitnesgroup);
            notebook.AddNote(fitnesandfriends);
            notebook.Display();

            Console.WriteLine("*******************************************************");
            notebook.ChangeTheme(darkomodo);
            notebook.Display();

            Console.WriteLine("*******************************************************");
            Notebook notebook2 = new Notebook(lighttheme);
            notebook2.AddNote(remindernote);
            notebook2.AddNote(fitnesgroup);
            notebook2.AddNote(fitnesandfriends);
            notebook2.Display();
        }
    }
}
