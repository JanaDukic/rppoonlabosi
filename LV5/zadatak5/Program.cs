﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lighttheme = new LightTheme();
            string message = "You have to put LV5 on git!\n";
            ReminderNote remindernote = new ReminderNote(message, lighttheme); //Note
            remindernote.Show();

            DarkMagentaTheme darkmagentatheme = new DarkMagentaTheme();
            remindernote = new ReminderNote(message, darkmagentatheme);
            remindernote.Show();
        }
    }
}
