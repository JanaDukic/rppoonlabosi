﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lighttheme = new LightTheme();
            string message = "This year Fitness members are: \n"; 
            FitnessGroupNote fitnesgroup = new FitnessGroupNote(message, lighttheme);
            fitnesgroup.AddAttendant("Jana");
            fitnesgroup.AddAttendant("David");
            fitnesgroup.RemoveAttendant("Jana");
            fitnesgroup.AddAttendant("Sven");
            fitnesgroup.Show();

            DarkMagentaTheme darkomodo = new DarkMagentaTheme();
            string message2 = "Bring your best friend to fitness training! \n";
            FitnessGroupNote fitnesandfriends = new FitnessGroupNote(message2, darkomodo);
            fitnesandfriends.AddAttendant("Jana i Rebeka");
            fitnesandfriends.AddAttendant("David i Danijel");
            fitnesandfriends.AddAttendant("Sven i Ivor");
            fitnesandfriends.RemoveAttendant("Sven i Ivor");
            fitnesandfriends.Show();
        }
    }
}
