﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class FitnessGroupNote : Note
    {
        private List<string> attendantnames;

        public FitnessGroupNote(string message, ITheme theme) : base(message, theme) {
            this.attendantnames = new List<string>();
            
        }

        public void AddAttendant(string name)
        {
            attendantnames.Add(name);
        }

        public void RemoveAttendant(string name)
        {
            attendantnames.Remove(name);
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("Fitness note: ");
            string framedMessage = this.GetFramedMessage();
  
            Console.WriteLine(framedMessage);
            foreach (string person in attendantnames)
            {
                Console.WriteLine(person);
            }
            Console.ResetColor();
        }
    }
}
