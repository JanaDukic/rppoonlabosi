﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace zadatak4
{
    class LoggingProxyDataset : IDataset
    {
        private Dataset dataset;
        private string filePath;
      

        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
            
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger instance = ConsoleLogger.GetInstance();
            instance.RecordLogging();
            
            if(dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();

        }

    }
}
