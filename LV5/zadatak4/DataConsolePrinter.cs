﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace zadatak4
{
    class DataConsolePrinter
    {
        public void Printer(IDataset argument)
        {
            ReadOnlyCollection<List<string>> data = argument.GetData();
            if (data != null) { 
                StringBuilder builder = new StringBuilder();
                foreach(List<string> item in data)
                {
                    builder.Clear();
                    foreach (string str in item)
                    {
                        builder.Append(str + " ");
                    }
                    Console.WriteLine(builder.ToString());
                }
                Console.WriteLine("\n");
            }
            else
            {
                Console.WriteLine("You are not allowed to see the Data, ProtectionProxy says\n");
            }

        }
    }
}
