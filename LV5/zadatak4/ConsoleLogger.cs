﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
       

        private ConsoleLogger()
        {
            
        }

        public static ConsoleLogger GetInstance()
        {
            if(instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }

        public void RecordLogging()
        {
            DateTime time = DateTime.Now;
            Console.WriteLine("Console Logger has recorded your logging at: " + time);
        }
    }

}
