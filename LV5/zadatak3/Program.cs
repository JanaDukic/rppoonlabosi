﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {

            DataConsolePrinter printer = new DataConsolePrinter();
            IDataset datasetFORvirtualproxy = new VirtualProxyDataset("C:\\Users\\Korisnik\\source\\repos\\LV5_RPPOON\\zadatak3\\CSV.txt");
            printer.Printer(datasetFORvirtualproxy);


            User user = User.GenerateUser("JANAuser");
            IDataset datasetFORprotectionProxy = new ProtectionProxyDataset(user);
            printer.Printer(datasetFORprotectionProxy);

            user = User.GenerateUser("NOTJANAuser");
            datasetFORprotectionProxy = new ProtectionProxyDataset(user);
            printer.Printer(datasetFORprotectionProxy);

            user = User.GenerateUser("user");
            datasetFORprotectionProxy = new ProtectionProxyDataset(user);
            printer.Printer(datasetFORprotectionProxy);
        }
    }
}
