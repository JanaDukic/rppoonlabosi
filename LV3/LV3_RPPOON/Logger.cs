﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    //zadatak 3
    class Logger
    {
        //Ako se datoteka postavi na jednom mjestu i kasnije radimo sa tom instancom koja je ista svuda i ako nismo
        //dirali setter(tj mijenjali datoteku), onda će se sadržaj zapisivati u istu datoteku.

        private static Logger instance;
        private string filePath;
        
        private Logger()
        {
            this.filePath = "C:\\Users\\Korisnik\\source\\repos\\LV3_RPPOON\\LV3_RPPOON\\CSVprimjer1.txt";
        }

        public static Logger GetInstance()
        {
            if(instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
       
        public void Logging(string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(text.ToString());
            }

        }

        public string FilePath
        {
            set { this.filePath = value; }
        }
    }
}
