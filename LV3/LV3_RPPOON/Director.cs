﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Director
    {
        IBuilder builder = new NotificationBuilder();

        public ConsoleNotification BuildNotification_ERROR(String author)
        {
            return builder.SetAuthor(author).SetTitle("Very good ERROR title").SetText("You have ERROR").SetColor(ConsoleColor.DarkMagenta)
                .SetLevel(Category.ERROR).Build();
        }

        public ConsoleNotification BuildNotification_ALERT(String author)
        {
            return builder.SetAuthor(author).SetTitle("Very good ALERT title").SetText("You have pi ALERT").SetColor(ConsoleColor.White)
                .SetLevel(Category.ALERT).Build();
        }

        public ConsoleNotification BuildNotification_INFO(String author)
        {
            return builder.SetAuthor(author).SetTitle("Very good INFO title").SetText("You have pi app INFO").SetColor(ConsoleColor.DarkYellow)
               .SetLevel(Category.INFO).Build();
        }

    }
}
