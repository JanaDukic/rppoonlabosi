﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //zadatak1
            Dataset data1 = new Dataset("C:\\Users\\Korisnik\\source\\repos\\LV3_RPPOON\\LV3_RPPOON\\CSVprimjer1.txt");
            Console.WriteLine(data1.PrintListElements());

            //zadatak2
            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.GenerateMatrix(5, 5);
            for(int i=0; i<5; i++)
            {
                for(int j=0; j<5; j++)
                {
                    Console.WriteLine(matrix[i][j] + " ");
                }
                Console.WriteLine("\n");
            }
                    


            //zadatak 3
            Logger loger = Logger.GetInstance();
            string text = "OVO ZELIM ZAPISATI U NOVU DATOTEKU, APENDANU";
            loger.FilePath = "C:\\Users\\Korisnik\\source\\repos\\LV3_RPPOON\\LV3_RPPOON\\zadatak3.txt";
            loger.Logging(text);

            //zadatk 4
            Category category = Category.INFO;
            ConsoleColor color = ConsoleColor.Cyan;
            ConsoleNotification notification = new ConsoleNotification("Jana Dukić", "Naslov je naslov", "Ovo je jako bitan tekst",
                new DateTime(2020, 4,23, 9, 24, 55),category , color );
            NotificationMAnager manager = new NotificationMAnager();
            manager.Display(notification);

            //zadatak 5
            IBuilder builder = new NotificationBuilder();
            NotificationMAnager manager2 = new NotificationMAnager();
            manager2.Display(builder.Build());

            //zadatak 6
            NotificationMAnager manager3 = new NotificationMAnager();
            Director director = new Director();
            manager3.Display(director.BuildNotification_ALERT("Marina"));
            manager3.Display(director.BuildNotification_ERROR("Petra"));
            manager3.Display(director.BuildNotification_INFO("David"));
            

        }
    }
}
