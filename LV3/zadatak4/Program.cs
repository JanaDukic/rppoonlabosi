﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            Category category = Category.INFO;
            ConsoleColor color = ConsoleColor.Cyan;
            ConsoleNotification notification = new ConsoleNotification("Jana Dukić", "Naslov je naslov", "Ovo je jako bitan tekst",
                new DateTime(2020, 4, 23, 9, 24, 55), category, color);
            NotificationMAnager manager = new NotificationMAnager();
            manager.Display(notification);
        }
    }
}
