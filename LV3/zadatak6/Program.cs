﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
    
            NotificationMAnager manager3 = new NotificationMAnager();
            Director director = new Director();
            manager3.Display(director.BuildNotification_ALERT("Marina"));
            manager3.Display(director.BuildNotification_ERROR("Petra"));
            manager3.Display(director.BuildNotification_INFO("David"));
        }
    }
}
