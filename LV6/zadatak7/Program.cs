﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker firstLink = new StringDigitChecker();
            StringLengthChecker lengthchecker = new StringLengthChecker(8);
            StringLowerCaseChecker lowerchecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();

            PasswordValidator validator = new PasswordValidator(firstLink);
            validator.AddNextLink(lengthchecker);
            validator.AddNextLink(lowerchecker);
            validator.AddNextLink(upperCaseChecker);

            Console.WriteLine(validator.isValidPassword("ovoJegr8"));
            Console.WriteLine(validator.isValidPassword("not4"));

        }
    }
}
