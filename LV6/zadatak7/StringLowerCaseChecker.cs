﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasLowerCase = false;
            foreach(char c in stringToCheck)
            {
                if (char.IsLower(c)) hasLowerCase = true;
            }
            
            return hasLowerCase;
        }

    }
}
