﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class StringUpperCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasUpperCase = false;
            foreach(char c in stringToCheck)
            {
                if (char.IsUpper(c)) hasUpperCase = true;
            }
           
            return hasUpperCase;
        }
    }
}
