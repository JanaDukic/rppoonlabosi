﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class PasswordValidator
    {
        StringChecker firstchecker;
        StringChecker lastadded;

        public PasswordValidator(StringChecker firstchecker)
        {
            this.firstchecker = firstchecker;
            this.lastadded = this.firstchecker;
        }

        public void AddNextLink(StringChecker nextchecker)
        {
            this.lastadded.SetNext(nextchecker);
            this.lastadded = nextchecker;
        }

        public bool isValidPassword(string password)
        {
            return firstchecker.Check(password);
        }
    
    }
}
