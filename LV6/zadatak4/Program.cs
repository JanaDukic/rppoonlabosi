﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount Janas = new BankAccount("Jana", "Osijek", 200);
            BankAccount Davids = new BankAccount("David", "Zagreb", 600);

            Memento MementoJanas = Janas.StoreState();
            Memento MementoDavids = Davids.StoreState();

            Janas.ChangeOwnerAddress("Banana");
            Console.WriteLine(Janas);

            Davids.UpdateBalance(1000);
            Console.WriteLine(Davids);

            Janas.RestoreState(MementoJanas);
            Console.WriteLine(Janas);
            Davids.RestoreState(MementoDavids);
            Console.WriteLine(Davids);

        }
    }
}
