﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Memento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }
        

        public Memento(string name, string address, decimal balance)
        {
            this.OwnerName = name;
            this.OwnerAddress = address;
            this.Balance = balance;
    
        }
    }
}
