﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class CareTaker
    {
        private Stack<Memento> stackstates;

        public CareTaker()
        {
            this.stackstates = new Stack<Memento>();
        }

        //public Memento PreviousState {
        //    get
        //    {
        //        return stackstates.Pop();
        //    }
        //    set
        //    {
        //        stackstates.Push(value);
        //    }                
        //}

        public void SaveState(Memento memento)
        {
            stackstates.Push(memento);
        }

        public void RemoveLastState()
        {
            stackstates.Pop();
        }

        public Memento SeeLastState()
        {
            return stackstates.Peek();
        }
    }
}
