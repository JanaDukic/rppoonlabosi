﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Program
    {
        
        static void Main(string[] args)
        {
            ToDoItem firstitem = new ToDoItem("RPPOON LV6", "predati na git i postaviti na private do 00", new DateTime(2020,5,14, 23,59,59));
            CareTaker caretaker = new CareTaker();

            //NAPOMENA: NAPRAVILA SAM PREKO SVOJSTVA, ALI NA NAČIN DA GET DOHVATI PREKO POPA PRETHODNO STANJE JER MI TO
            //IMA VIŠE SMISLA, NEGO DA SVAKI PUT MORAM MICAT POSEBNO RUČNO PRETHODNO STANJE
            //caretaker.PreviousState = firstitem.StoreState();
            //Console.WriteLine(firstitem);

            //firstitem.Rename("RPPOON LV7");
            //caretaker.PreviousState = firstitem.StoreState();
            //Console.WriteLine(firstitem);

            //firstitem.Rename("*****************");
            //Console.WriteLine(firstitem);

            //firstitem.RestoreState(caretaker.PreviousState);
            //Console.WriteLine(firstitem);

            //firstitem.RestoreState(caretaker.PreviousState);
            //Console.WriteLine(firstitem);

            caretaker.SaveState(firstitem.StoreState());
            Console.WriteLine(firstitem);

            firstitem.Rename("RPPOON LV7");
            caretaker.SaveState(firstitem.StoreState());
            Console.WriteLine(firstitem);

            firstitem.Rename("*****************");
            Console.WriteLine(firstitem);
         
            firstitem.RestoreState(caretaker.SeeLastState());
            Console.WriteLine(firstitem);

            caretaker.RemoveLastState();

            firstitem.RestoreState(caretaker.SeeLastState());
            Console.WriteLine(firstitem);

        }
    }
}
