﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker checker = new StringDigitChecker();
            StringLengthChecker lengthchecker = new StringLengthChecker(8);
            StringLowerCaseChecker lowercasechecher = new StringLowerCaseChecker();
            StringUpperCaseChecker uppercasechecker = new StringUpperCaseChecker();

            checker.SetNext(lengthchecker);
            lengthchecker.SetNext(lowercasechecher);
            lowercasechecher.SetNext(uppercasechecker);

            Console.WriteLine(checker.Check("oVojegr8"));
            Console.WriteLine(checker.Check("notgutsifrA"));


        }
    }
}
