﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class StringLengthChecker : StringChecker
    {
        private int length;
        
        public StringLengthChecker(int length)
        {
            this.length = length;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            bool isLengthOK = false;
            if (stringToCheck.Length >= this.length) isLengthOK = true;
            return isLengthOK;
        }
    }
}
