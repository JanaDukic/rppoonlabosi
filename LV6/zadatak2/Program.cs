﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> listOfProducts = new List<Product>{ new Product("Pen", 15.99), new Product("Notebook", 6.99)};
            Box superbox = new Box(listOfProducts);
            IAbstractIterator iterator = superbox.GetIterator();

            while(iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current);
                iterator.Next();
            }
        

        }
    }
}
