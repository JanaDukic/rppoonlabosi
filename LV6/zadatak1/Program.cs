﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            Note firstnote = new Note("Prva biljeska", "Ovo je tekst prve biljeske");
            Note secondnote = new Note("Druga biljeska", "Ovo je tekst druge biljeske");
            Note thirdnote = new Note("Treca biljeska", "Ovo je tekst trece biljeske");
            Note fourthnote = new Note("Cetvrta biljeska", "Ovo je tekst cetvrte biljeske");

            notebook.AddNote(firstnote);
            notebook.AddNote(secondnote);
            notebook.AddNote(thirdnote);
            notebook.AddNote(fourthnote);

            IAbstractIterator iterator = notebook.GetIterator();
            notebook.RemoveNote(secondnote);

            for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
        }
    }
}
