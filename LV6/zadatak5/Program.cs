﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class Program
    {
        
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "C:\\Users\\Korisnik\\source\\repos\\LV6_RPPOON\\zadatak5\\logFile.txt");
            logger.SetNextLogger(fileLogger); 
            logger.Log("informirajuca poruka", MessageType.INFO);
            logger.Log("error poruka", MessageType.ERROR);
            logger.Log("upozoravajuća poruka", MessageType.WARNING);
           
                                          
        }
    }
}
