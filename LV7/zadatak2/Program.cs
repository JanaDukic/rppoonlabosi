﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[10] { 1.1, 5.5, 2.2, 9.9, 8.8, 4.4, 0.9, 7.7, 3.3, 6.6 };
            NumberSequence numberSequence = new NumberSequence(array);
          
            Console.WriteLine("Zadani brojevi: \n" + numberSequence);

            SearchStrategy sequentialstrategy = new SequentialSearch();
            numberSequence.SetSearchStrategy(sequentialstrategy);
            Console.WriteLine("Number 9.8 is in the array: " + numberSequence.Search(9.8));
            Console.WriteLine("Number 9.9 is in the array: " + numberSequence.Search(9.9));

            
        }
    }
}
