﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] array , double x);

    }
}
