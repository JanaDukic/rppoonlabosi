﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak2
{
    class SequentialSearch : SearchStrategy
    {
        public override bool Search(double[] array, double x)
        {
            int arraySize = array.Length;
            for(int i=0; i< arraySize; i++)
            {
                if(array[i] == x)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
