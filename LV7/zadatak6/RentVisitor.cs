﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class RentVisitor : IVisitor
    {
        private const double rent = 0.1;

        public double Visit(DVD DVDItem)
        {
            if(DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }
            return  DVDItem.Price * rent;
        }

        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * rent;
        }

        public double Visit(Book BookItem)
        {
            return BookItem.Price * rent;
        }
    }
}
