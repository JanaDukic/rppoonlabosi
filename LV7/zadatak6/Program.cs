﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem DVD = new DVD("Pocahontas", DVDType.MOVIE, 99.99);
            IItem DVDS = new DVD("C#", DVDType.SOFTWARE, 250);
            IItem VHS = new VHS("Player", 200.01);
            IItem Book = new Book("All I want", 69.99);

            RentVisitor visitor = new RentVisitor();

            Console.WriteLine(DVD.Accept(visitor));
            Console.WriteLine(DVDS.Accept(visitor));
            Console.WriteLine(VHS.Accept(visitor));
            Console.WriteLine(Book.Accept(visitor));
        }
    }
}
