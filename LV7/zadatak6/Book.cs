﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Book : IItem
    {
        public string Title { get; private set; }
        public double Price { get; private set; }

        public Book(string title, double price)
        {
            this.Title = title;
            this.Price = price;
        }

        public override string ToString()
        {
            return "BOOK: " + this.Title + Environment.NewLine + " -> Price: " + this.Price;
        }

        public double Accept(IVisitor visitor) { return visitor.Visit(this); }
    }
}
