﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem DVD = new DVD("Pocahontas", DVDType.MOVIE , 99.99);
            IItem VHS = new VHS("Player", 200.01);
            IItem Book = new Book("All I want", 69.99);

            BuyVisitor visitor = new BuyVisitor();

            Console.WriteLine(DVD.Accept(visitor));
            Console.WriteLine(VHS.Accept(visitor));
            Console.WriteLine(Book.Accept(visitor));
            
        }
    }
}
