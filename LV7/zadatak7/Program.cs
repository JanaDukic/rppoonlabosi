﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem DVD = new DVD("Pocahontas", DVDType.MOVIE, 99.99);
            IItem DVDS = new DVD("C#", DVDType.SOFTWARE, 250);
            IItem VHS = new VHS("Player", 200.01);
            IItem Book = new Book("All I want", 69.99);

            RentVisitor visitor = new RentVisitor();
            BuyVisitor buyvisitor = new BuyVisitor();

            Cart mycart = new Cart();
            mycart.AddToCart(DVD);
            mycart.AddToCart(DVDS);
            mycart.AddToCart(VHS);
            mycart.AddToCart(Book);
            
            Console.WriteLine("Visitor in cart: " + mycart.Accept(visitor));
            Console.WriteLine("Buyer in cart: " + mycart.Accept(buyvisitor));

            mycart.RemoveFromCart(Book);
            Console.WriteLine("Visitor in cart: " + mycart.Accept(visitor));
            Console.WriteLine("Buyer in cart: " + mycart.Accept(buyvisitor));
        }
    }
}
