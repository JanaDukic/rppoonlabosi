﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Cart: IItem
    {
        private List<IItem> cart;

        public Cart()
        {
            cart = new List<IItem>();
        }

        public void AddToCart(IItem item)
        {
            cart.Add(item);
        }

        public void RemoveFromCart(IItem item)
        {
            cart.Remove(item);
        }

        public double Accept(IVisitor visitor)
        {
            double sum = 0.0;
            foreach(IItem item in cart)
            {
                sum += item.Accept(visitor);
            }
            
            return sum;
        }

    }
}
