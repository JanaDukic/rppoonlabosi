﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            double[] array = new double[10]{ 1.1, 5.5, 2.2, 9.9, 8.8, 4.4, 0.9, 7.7, 3.3, 6.6 };
            double[] array2 = new double[10] { 1.1, 5.5, 2.2, 9.9, 8.8, 4.4, 0.9, 7.7, 3.3, 6.6 };
            double[] array3 = new double[10] { 1.1, 5.5, 2.2, 9.9, 8.8, 4.4, 0.9, 7.7, 3.3, 6.6 };

            NumberSequence numberSequence = new NumberSequence(array);
            NumberSequence numberSequence2 = new NumberSequence(array2);
            NumberSequence numberSequence3 = new NumberSequence(array3);

            Console.WriteLine(numberSequence);
            Console.WriteLine(numberSequence2);
            Console.WriteLine(numberSequence3);

            SortStrategy bubblesort = new BubbleSort();
            SortStrategy combsort = new CombSort();
            SortStrategy sequentialsort = new SequentialSort();

            numberSequence.SetSortStrategy(bubblesort);
            numberSequence.Sort();
            Console.WriteLine("BUBBLE: \n" + numberSequence);

            numberSequence2.SetSortStrategy(combsort);
            numberSequence2.Sort();
            Console.WriteLine("COMB: \n" +numberSequence2);

            numberSequence3.SetSortStrategy(sequentialsort);
            numberSequence3.Sort();
            Console.WriteLine("SEQUENTIAL: \n" + numberSequence3);

        }
    }
}
