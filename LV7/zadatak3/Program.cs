﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {

            SystemDataProvider systemDataProvider = new SystemDataProvider();
            Logger filelogger = new FileLogger("C:\\Users\\Korisnik\\source\\repos\\LV7_RPPOON\\zadatak3\\TextFile.txt");
            Logger consolelogger = new ConsoleLogger();

            systemDataProvider.Attach(consolelogger);
            systemDataProvider.Attach(filelogger);

            while (true)
            {
                systemDataProvider.GetCPULoad();
                systemDataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }


        }
    }
}
